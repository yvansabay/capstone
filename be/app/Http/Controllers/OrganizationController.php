<?php

namespace App\Http\Controllers;

use App\Models\OrganizationUser;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Gate;

class OrganizationController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth:api');
    }

    public function index(){
        if (!Gate::allows('access_organization')) {
            return response()->json(['msg' => 'User has no permission'], 422);
        }

        $org = OrganizationUser::whereHas('organization', function($query){
            $query->where('department_id', auth('api')->user()->userinfo->department->id);
        })->with(['user', 'user.userinfo', 'organization', 'user.userinfo.role'])->paginate(8);

        return response()->json($org);
    }



}
