<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Organization extends Model
{
    use HasFactory;
    public $guarded = [];

    public function department(){
        return $this->belongsTo(Department::class, 'department_id', 'id');
    }
}
